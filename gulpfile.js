'use strict';

var gulp = require('gulp');

gulp.task('default', function() {
  console.log("Gulp is working!");
});

gulp.task('build:dev', function() {
  gulp.src('bower_components/angular/angular.min.js')
      .pipe(gulp.dest('core/angular/'));
  gulp.src('bower_components/bootstrap/dist/css/bootstrap.min.css')
      .pipe(gulp.dest('theme/styles/'));
  gulp.src('bower_components/bootstrap/dist/js/bootstrap.min.js')
      .pipe(gulp.dest('theme/scripts/'));
  gulp.src('bower_components/font-awesome/css/font-awesome.min.css')
      .pipe(gulp.dest('theme/styles/'));
  gulp.src('bower_components/font-awesome/fonts/*')
      .pipe(gulp.dest('theme/fonts/'));
  gulp.src('bower_components/jquery/dist/jquery.min.js')
      .pipe(gulp.dest('theme/scripts/'));
  gulp.src('bower_components/tether/dist/css/tether.min.css')
      .pipe(gulp.dest('theme/styles/'));
  gulp.src('bower_components/tether/dist/js/tether.min.js')
      .pipe(gulp.dest('theme/scripts/'));
});
